# yamls

Este é um repositório criado para centralizar e manter atualizados manifestos Kubernetes e arquivos de configuração, sendo utilizado como guia de consulta.

## Kubernetes Dashboard

[Github Repo](https://github.com/kubernetes/dashboard)

```yml
# Simple user (admin-user.yaml)
apiVersion: v1
kind: ServiceAccount
metadata:
  name: admin-user
  namespace: kubernetes-dashboard
```
---
```yml
# RBAC user (dashboard-adminuser.yaml)
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: admin-user
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
- kind: ServiceAccount
  name: admin-user
  namespace: kubernetes-dashboard
```

Access: [http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/](http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/)

## Ansible

### oh-my-zsh

Instalação do **oh-my-zsh** com [Ansible](https://www.ansible.com/) Galaxy
[gantsign.antigen](https://galaxy.ansible.com/gantsign/antigen).

`$ ansible-galaxy install gantsign.antigen`

```yml
# ohmyzsh.yaml
- hosts: servers
  roles:
    - role: gantsign.antigen
      users:
        - username: example
          antigen_libraries:
            - name: oh-my-zsh
          antigen_theme:
            name: robbyrussell
          antigen_bundles:
            # Bundles from the default repo (robbyrussell's oh-my-zsh).
            - name: git
            - name: heroku
            - name: pip
            - name: lein
            - name: command-not-found
            # Syntax highlighting bundle.
            - name: zsh-syntax-highlighting # `name` is required (any valid file name will do so long as it's unique for the bundles)
              url: zsh-users/zsh-syntax-highlighting
```

`$ ansible-playbook --private-key ~/.ssh/id_rsa -u paulo -i hosts ohmyzsh.yaml`

## Docker

### Monitoramento de Host Docker

O projeto [Dockprom](https://github.com/stefanprodan/dockprom) utiliza o Prometheus e o Grafana para disponibilizar ao sysadmin uma visão completa de monitoramento dos recursos da máquina host Docker e dos containers, inclui também já alertas pré configurados e integração com o Slack.